/* global QUnit */

sap.ui.require(["zvcfui/mroruleconfig/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
